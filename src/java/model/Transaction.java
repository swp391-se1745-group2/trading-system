/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package model;

import java.util.Date;

/**
 *
 * @author Admin
 */
public class Transaction {
    private Long id;
    private Long userId;
    private Long orderId;
    private String code;
    private Integer type;
    private Integer mode;
    private Integer status;
    private Date createdAt;
    private Date updatedAt;
    private String content;
}
