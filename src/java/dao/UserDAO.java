/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package dao;

import model.User;
import java.sql.*;
import util.DbUtil;

/**
 *
 * @author Admin
 */
public class UserDAO {

    private static final String CHECK_LOGIN_SQL = "SELECT * FROM user WHERE email = ? AND passwordHash = ?";
    private static final String REGISTER_USER_SQL = "INSERT INTO user (lastName, email, passwordHash, registeredAt) VALUES (?, ?, ?, ?)";
    
    public User register(String lastName, String email, String password){
    try (Connection connection = DbUtil.getConnection(); PreparedStatement statement = connection.prepareStatement(REGISTER_USER_SQL)) {

            statement.setString(1, lastName);
            statement.setString(2, email);
            statement.setString(3, password);
            statement.setTimestamp(4, new Timestamp(System.currentTimeMillis()));
            
            statement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
            // Handle the exception appropriately (log, throw, etc.)
        }

        return null;
    }
    
    public User checkLogin(String email, String password) {
        try (Connection connection = DbUtil.getConnection(); PreparedStatement statement = connection.prepareStatement(CHECK_LOGIN_SQL)) {

            statement.setString(1, email);
            statement.setString(2, password);

            try (ResultSet resultSet = statement.executeQuery()) {
                if (resultSet.next()) {
                    return mapResultSetToUser(resultSet);
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
            // Handle the exception appropriately (log, throw, etc.)
        }

        return null;
    }

    private User mapResultSetToUser(ResultSet resultSet) throws SQLException {
        User user = new User();
        user.setId(resultSet.getLong("id"));
        user.setFirstName(resultSet.getString("firstName"));
        user.setMiddleName(resultSet.getString("middleName"));
        user.setLastName(resultSet.getString("lastName"));
        user.setMobile(resultSet.getString("mobile"));
        user.setEmail(resultSet.getString("email"));
        user.setPasswordHash(resultSet.getString("passwordHash"));
        user.setAdmin(resultSet.getBoolean("admin"));
        user.setVendor(resultSet.getBoolean("vendor"));
        user.setRegisteredAt(resultSet.getTimestamp("registeredAt"));
        user.setLastLogin(resultSet.getTimestamp("lastLogin"));
        return user;
    }
}
