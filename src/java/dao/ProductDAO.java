/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package dao;

import java.util.ArrayList;
import java.util.List;
import model.Product;
import util.DbUtil;
import java.sql.*;

/**
 *
 * @author Admin
 */
public class ProductDAO {
    private static final String INSERT_PRODUCT_SQL = "INSERT INTO product (title, slug, summary, type, sku, price, discount, quantity, shopId, createdAt, updatedAt, publishedAt, startsdAt, endsAt, content) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";
    private static final String SELECT_ALL_PRODUCTS_SQL = "SELECT * FROM product";
    private static final String SELECT_PRODUCTS_BY_CATEGORY_SQL = "SELECT * FROM product WHERE type = ?";
    private static final String SELECT_PRODUCT_BY_ID_SQL = "SELECT * FROM product WHERE id = ?";
    private static final String SEARCH_PRODUCTS_SQL = "SELECT * FROM product WHERE title LIKE ? OR type LIKE ?";
    
    public void addProduct(Product p) {
        try (Connection connection = DbUtil.getConnection();
             PreparedStatement preparedStatement = connection.prepareStatement(INSERT_PRODUCT_SQL)) {

            preparedStatement.setString(1, p.getTitle());
            preparedStatement.setString(2, p.getSlug());
            preparedStatement.setString(3, p.getSummary());
            preparedStatement.setInt(4, p.getType());
            preparedStatement.setString(5, p.getSku());
            preparedStatement.setFloat(6, p.getPrice());
            preparedStatement.setFloat(7, p.getDiscount());
            preparedStatement.setInt(8, p.getQuantity());
            preparedStatement.setLong(9, p.getShopId());
            preparedStatement.setTimestamp(10, new Timestamp(System.currentTimeMillis())); // createdAt
            preparedStatement.setTimestamp(11, new Timestamp(System.currentTimeMillis())); // updatedAt
            preparedStatement.setTimestamp(12, new Timestamp(System.currentTimeMillis())); // publishedAt
            preparedStatement.setTimestamp(13, new Timestamp(System.currentTimeMillis())); // startsdAt
            preparedStatement.setTimestamp(14, new Timestamp(System.currentTimeMillis())); // endsAt
            preparedStatement.setString(15, p.getContent());

            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
            // Handle the exception appropriately (log, throw, etc.)
        }
    }

    public List<Product> getList() {
        List<Product> productList = new ArrayList<>();

        try (Connection connection = DbUtil.getConnection();
             Statement statement = connection.createStatement();
             ResultSet resultSet = statement.executeQuery(SELECT_ALL_PRODUCTS_SQL)) {

            while (resultSet.next()) {
                Product product = mapResultSetToProduct(resultSet);
                productList.add(product);
            }
        } catch (SQLException e) {
            e.printStackTrace();
            // Handle the exception appropriately (log, throw, etc.)
        }

        return productList;
    }

    public List<Product> getListByCategory(int ma_the_loai) {
        List<Product> productList = new ArrayList<>();

        try (Connection connection = DbUtil.getConnection();
             PreparedStatement preparedStatement = connection.prepareStatement(SELECT_PRODUCTS_BY_CATEGORY_SQL)) {

            preparedStatement.setInt(1, ma_the_loai);

            try (ResultSet resultSet = preparedStatement.executeQuery()) {
                while (resultSet.next()) {
                    Product product = mapResultSetToProduct(resultSet);
                    productList.add(product);
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
            // Handle the exception appropriately (log, throw, etc.)
        }

        return productList;
    }

    public Product getProduct(int ma_san_pham) {
        try (Connection connection = DbUtil.getConnection();
             PreparedStatement preparedStatement = connection.prepareStatement(SELECT_PRODUCT_BY_ID_SQL)) {

            preparedStatement.setInt(1, ma_san_pham);

            try (ResultSet resultSet = preparedStatement.executeQuery()) {
                if (resultSet.next()) {
                    return mapResultSetToProduct(resultSet);
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
            // Handle the exception appropriately (log, throw, etc.)
        }

        return null;
    }

    public List<Product> searchList(String ten_san_pham, String ten_the_loai) {
        List<Product> productList = new ArrayList<>();

        try (Connection connection = DbUtil.getConnection();
             PreparedStatement preparedStatement = connection.prepareStatement(SEARCH_PRODUCTS_SQL)) {

            preparedStatement.setString(1, "%" + ten_san_pham + "%");
            preparedStatement.setString(2, "%" + ten_the_loai + "%");

            try (ResultSet resultSet = preparedStatement.executeQuery()) {
                while (resultSet.next()) {
                    Product product = mapResultSetToProduct(resultSet);
                    productList.add(product);
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
            // Handle the exception appropriately (log, throw, etc.)
        }

        return productList;
    }

    private Product mapResultSetToProduct(ResultSet resultSet) throws SQLException {
        // Map the ResultSet to a Product object
        Product product = new Product();
        product.setId(resultSet.getLong("id"));
        product.setTitle(resultSet.getString("title"));
        product.setSlug(resultSet.getString("slug"));
        product.setSummary(resultSet.getString("summary"));
        product.setType(resultSet.getInt("type"));
        product.setSku(resultSet.getString("sku"));
        product.setPrice(resultSet.getFloat("price"));
        product.setDiscount(resultSet.getFloat("discount"));
        product.setQuantity(resultSet.getInt("quantity"));
        product.setShopId(resultSet.getLong("shopId"));
        product.setCreatedAt(resultSet.getTimestamp("createdAt"));
        product.setUpdatedAt(resultSet.getTimestamp("updatedAt"));
        product.setPublishedAt(resultSet.getTimestamp("publishedAt"));
        product.setStartsdAt(resultSet.getTimestamp("startsAt"));
        product.setEndsAt(resultSet.getTimestamp("endsAt"));
        product.setContent(resultSet.getString("content"));
        return product;
    }
}
