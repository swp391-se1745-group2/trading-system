<%-- 
    Document   : register
    Created on : Jan 27, 2024, 11:48:36 AM
    Author     : Admin
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <script src="https://cdn.tailwindcss.com"></script>
        <title>Register Page</title>
    </head>
    <body class="flex items-center justify-center h-screen">
        <div class="p-10">
            <h1 class="mb-8 font-extrabold text-4xl">Register</h1>
            <div class="grid grid-cols-1 md:grid-cols-2 gap-8">

                <form action="MainController" method="POST">
                    <div>
                        <label class="block font-semibold" for="lastName">Name</label>
                        <input class="w-full shadow-inner bg-gray-100 rounded-lg placeholder-black text-2xl p-4 border-none block mt-1 w-full" id="lastName" type="text" name="lastName" required="required" autofocus="autofocus">
                    </div>

                    <div class="mt-4">
                        <label class="block font-semibold" for="email">Email</label>
                        <input class="w-full shadow-inner bg-gray-100 rounded-lg placeholder-black text-2xl p-4 border-none block mt-1 w-full" id="email" type="email" name="email" required="required">
                    </div>

                    <div class="mt-4">
                        <label class="block font-semibold" for="password">Password</label>
                        <input class="w-full shadow-inner bg-gray-100 rounded-lg placeholder-black text-2xl p-4 border-none block mt-1 w-full" id="password" type="password" name="password" required="required" autocomplete="new-password">
                    </div>
                    <% String registerError = request.getParameter("registerError"); %>
                    <% if (registerError != null && !registerError.isEmpty()) {%>
                        <p class="text-center text-sm text-red-500"><%= registerError%></p>
                    <% }%>
                    <div class="flex items-center justify-between mt-8">
                        <button type="submit" name="btnAction" value="Register" class="flex items-center justify-center px-8 py-3 border border-transparent text-base font-medium rounded-md text-white bg-black focus:bg-gray-600 focus:outline-none md:py-4 md:text-lg md:px-10">Register</button>
                        <a class="font-semibold" href="login">
                            Already registered?
                        </a>
                    </div>
                </form>

                <aside class="">
                    <div class="bg-gray-100 p-8 rounded">
                        <h2 class="font-bold text-2xl">Instructions</h2>
                        <ul class="list-disc mt-4 list-inside">
                            <li>All users must provide a valid email address and password to create an account.</li>
                            <li>Users must not use offensive, vulgar, or otherwise inappropriate language in their username or profile information</li>
                            <li>Users must not create multiple accounts for the same person.</li>
                        </ul>
                    </div>
                </aside>

            </div>
        </div>
    </body>
</html>
