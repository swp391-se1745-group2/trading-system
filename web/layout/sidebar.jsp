<%@page contentType="text/html" pageEncoding="UTF-8"%>
<style>
    /* CSS cho submenu */
    .submenu {
        display: none;
        padding-left: 20px; /* Để có khoảng cách so với mục cha */
    }

    .submenu a {
        color: #ccc;
    }

    .submenu a:hover {
        color: #fff;
    }
</style>
<aside
    class="sidebar w-64 md:shadow transform -translate-x-full md:translate-x-0 transition-transform duration-150 ease-in bg-black"
    >
    <div class="sidebar-header flex items-center justify-center py-4">
        <div class="inline-flex">
            <a href="#" class="inline-flex flex-row items-center">
                <svg class="w-10 h-10 text-red-400" fill="currentColor" viewBox="0 0 20 20">
                <path
                    fill-rule="evenodd"
                    d="M11.757 2.034a1 1 0 01.638.519c.483.967.844 1.554 1.207 2.03.368.482.756.876 1.348 1.467A6.985 6.985 0 0117 11a7.002 7.002 0 01-14 0c0-1.79.684-3.583 2.05-4.95a1 1 0 011.707.707c0 1.12.07 1.973.398 2.654.18.374.461.74.945 1.067.116-1.061.328-2.354.614-3.58.225-.966.505-1.93.839-2.734.167-.403.356-.785.57-1.116.208-.322.476-.649.822-.88a1 1 0 01.812-.134zm.364 13.087A2.998 2.998 0 017 13s.879.5 2.5.5c0-1 .5-4 1.25-4.5.5 1 .786 1.293 1.371 1.879.586.585.879 1.353.879 2.121s-.293 1.536-.879 2.121z"
                    clip-rule="evenodd"
                    />
                </svg>
                <span class="leading-10 text-gray-100 text-2xl font-bold ml-1 uppercase">FPT SHOP</span>
            </a>
        </div>
    </div>
    <div class="sidebar-content px-4 py-6">
        <ul class="flex flex-col w-full">
            <li class="my-px">
                <a
                    href="/Shoping/index.jsp"
                    class="flex flex-row items-center h-10 px-3 rounded-lg <%= request.getRequestURI().endsWith("index.jsp") ? "text-gray-700 bg-gray-100" : "text-gray-300 hover:bg-gray-100 hover:text-gray-700"%>"
                    >
                    <i class="w-8 m-1 fa-solid fa-house"></i>
                    <span class="ml-3">Trang chủ</span>
                </a>
            </li>
            <li class="my-px">
                <span class="flex font-medium text-sm text-gray-300 px-4 my-4 uppercase">Projects</span>
            </li>
            <li class="my-px has-submenu">
                <a class="cursor-pointer flex flex-row items-center h-10 px-3 rounded-lg text-gray-300 hover:bg-gray-100 hover:text-gray-700">
                    <i class="w-8 m-1 fa-solid fa-credit-card"></i>
                    <span class="ml-3">Quản lý thanh toán</span>
                </a>
                <ul class="submenu">
                    <li class="my-px">
                        <a
                            href="#"
                            class="flex flex-row items-center h-10 px-3 rounded-lg text-gray-300 hover:bg-gray-100 hover:text-gray-700"
                            >

                            <i class="w-8 m-1 fa-brands fa-cc-visa"></i>
                            <span class="ml-3">Nạp tiền</span>
                        </a>
                    </li>
                    <li class="my-px">
                        <a
                            href="#"
                            class="flex flex-row items-center h-10 px-3 rounded-lg text-gray-300 hover:bg-gray-100 hover:text-gray-700"
                            >

                            <i class="w-8 m-1 fa-solid fa-right-left"></i>
                            <span class="ml-3">Lịch sử giao dịch</span>

                        </a>
                    </li>
                    <li class="my-px">
                        <a
                            href="#"
                            class="flex flex-row items-center h-10 px-3 rounded-lg text-gray-300 hover:bg-gray-100 hover:text-gray-700"
                            >

                            <i class="w-8 m-1 fa-solid fa-paper-plane"></i>
                            <span class="ml-3">Yêu cầu rút tiền</span>

                        </a>
                    </li>
                </ul>
            </li>
            <li class="my-px has-submenu">
                <a
                    class="cursor-pointer flex flex-row items-center h-10 px-3 rounded-lg text-gray-300 hover:bg-gray-100 hover:text-gray-700"
                    >

                    <i class="w-8 m-1 fa-brands fa-product-hunt"></i>
                    <span class="ml-3">Mua hàng</span>
                </a>
                <ul class="submenu product listorder">
                    <li class="my-px">
                        <a
                            href="/Shoping/product"
                            class="flex flex-row items-center h-10 px-3 rounded-lg <%= request.getRequestURI().endsWith("product") ? "!text-gray-700 !bg-gray-100" : "!text-gray-300 hover:!bg-gray-100 hover:!text-gray-700"%>"
                            >

                            <i class="w-8 m-1 fa-brands fa-product-hunt"></i>
                            <span class="ml-3">Sản phẩm</span>
                        </a>
                    </li>
                    <li class="my-px">
                        <a
                            href="/Shoping/listorder"
                            class="flex flex-row items-center h-10 px-3 rounded-lg <%= request.getRequestURI().endsWith("listorder") ? "!text-gray-700 !bg-gray-100" : "!text-gray-300 hover:!bg-gray-100 hover:!text-gray-700"%>"
                            >
                            <i class="w-8 m-1 fa-solid fa-list"></i>
                            <span class="ml-3">Đơn hàng</span>

                        </a>
                    </li>

                </ul>
            </li>
            <li class="my-px has-submenu">
                <a
                    class="cursor-pointer flex flex-row items-center h-10 px-3 rounded-lg text-gray-300 hover:bg-gray-100 hover:text-gray-700"
                    >
                    <i class="w-8 m-1 fa-regular fa-handshake"></i>

                    <span class="ml-3">Trung gian</span>
                </a>
                <ul class="submenu market">
                    <li class="my-px">
                        <a
                            href="/Shoping/market"
                            class="flex flex-row items-center h-10 px-3 rounded-lg <%= request.getRequestURI().endsWith("market") ? "!text-gray-700 !bg-gray-100" : "!text-gray-300 hover:!bg-gray-100 hover:!text-gray-700"%>"
                            >

                            <i class="w-8 m-1 fa-solid fa-cart-shopping"></i>
                            <span class="ml-3">Chợ công khai</span>
                        </a>
                    </li>
                    <li class="my-px">
                        <a
                            href="#"
                            class="flex flex-row items-center h-10 px-3 rounded-lg text-gray-300 hover:bg-gray-100 hover:text-gray-700"
                            >
                            <i class="w-8 m-1 fa-solid fa-cart-shopping"></i>
                            <span class="ml-3">Đơn bán của tôi</span>

                        </a>
                    </li>
                    <li class="my-px">
                        <a
                            href="#"
                            class="flex flex-row items-center h-10 px-3 rounded-lg text-gray-300 hover:bg-gray-100 hover:text-gray-700"
                            >
                            <i class="w-8 m-1 fa-solid fa-cart-shopping"></i>
                            <span class="ml-3">Đơn mua của tôi</span>

                        </a>
                    </li>

                </ul>
            </li>
            <li class="my-px">
                <span class="flex font-medium text-sm text-gray-300 px-4 my-4 uppercase">Account</span>
            </li>
            <li class="my-px">
                <a
                    href="/Shoping/profile"
                    class="flex flex-row items-center h-10 px-3 rounded-lg <%= request.getRequestURI().endsWith("profile") ? "!text-gray-700 !bg-gray-100" : "!text-gray-300 hover:!bg-gray-100 hover:!text-gray-700"%>"
                    >
                    <i class="w-8 m-1 fa-regular fa-user"></i>
                    <span class="ml-3">Profile</span>
                </a>
            </li>
            <li class="my-px">
                <a
                    href="#"
                    class="flex flex-row items-center h-10 px-3 rounded-lg text-gray-300 hover:bg-gray-100 hover:text-gray-700"
                    >
                    <i class="w-8 m-1 fa-regular fa-bell"></i>
                    <span class="ml-3">Notifications</span>
                    <span
                        class="flex items-center justify-center text-xs text-red-500 font-semibold bg-red-100 h-6 px-2 rounded-full ml-auto"
                        >10</span>
                </a>
            </li>
            <li class="my-px">
                <a
                    href="#"
                    class="flex flex-row items-center h-10 px-3 rounded-lg text-gray-300 hover:bg-gray-100 hover:text-gray-700"
                    >
                    <i class="w-8 m-1 fa-solid fa-gear"></i>
                    <span class="ml-3">Settings</span>
                </a>
            </li>
            <li class="my-px">
                <a
                    href="#"
                    class="flex flex-row items-center h-10 px-3 rounded-lg text-gray-300 hover:bg-gray-100 hover:text-gray-700"
                    >
                    <i class="w-8 m-1 fa-solid fa-lock"></i>
                    <span class="ml-3">Logout</span>
                </a>
            </li>
        </ul>
    </div>
</aside>
<script>
    document.addEventListener('DOMContentLoaded', function () {
        var menuItems = document.querySelectorAll('.has-submenu');

        menuItems.forEach(function (item) {
            item.addEventListener('click', function () {
                var submenu = this.querySelector('.submenu');

                // Toggle lớp hiển thị khi click vào mục menu có submenu
                if (submenu) {
                    submenu.style.display = (submenu.style.display === 'block') ? 'none' : 'block';
                }
            });
        });
        var currentURL = window.location.href;
        if (currentURL.endsWith("/product")) {
            var muaHangSubmenu = document.querySelector('.has-submenu .product');
            if (muaHangSubmenu) {
                muaHangSubmenu.style.display = 'block';
            }
        }
        if (currentURL.endsWith("/listorder")) {
            var muaHangSubmenu = document.querySelector('.has-submenu .listorder');
            if (muaHangSubmenu) {
                muaHangSubmenu.style.display = 'block';
            }
        }
        if (currentURL.endsWith("/market")) {
            var muaHangSubmenu = document.querySelector('.has-submenu .market');
            if (muaHangSubmenu) {
                muaHangSubmenu.style.display = 'block';
            }
        }
    });
</script>  