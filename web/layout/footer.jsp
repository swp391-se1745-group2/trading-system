<%-- 
    Document   : footer.jsp
    Created on : Jan 27, 2024, 2:34:56 PM
    Author     : Admin
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<footer class="footer px-4 py-6">
    <div class="footer-content">
        <p class="text-sm text-gray-600 text-center">© V2P 2024. All for you. <a href="https://twitter.com/iaminos">by THAV</a></p>
    </div>
</footer>
