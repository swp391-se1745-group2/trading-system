<%-- 
    Document   : product
    Created on : Jan 27, 2024, 3:18:30 PM
    Author     : Admin
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <script src="https://cdn.tailwindcss.com"></script>
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.5.1/css/all.min.css" integrity="sha512-DTOQO9RWCH3ppGqcWaEA1BIZOC6xxalwEsw9c2QQeAIftl+Vegovlnee1c9QX4TctnWMn13TZye+giMm8e2LwA==" crossorigin="anonymous" referrerpolicy="no-referrer" />
        <title>Product Page</title>
    </head>
    <body style="height: min-content;" class="my-auto w-full">
        <div class="flex flex-row min-h-screen bg-gray-100 text-gray-800">
            <%@ include file="/layout/sidebar.jsp" %>
            <main class="main flex flex-col flex-grow -ml-64 md:ml-0 transition-all duration-150 ease-in">
                <%@ include file="/layout/header.jsp" %>
                <div class="main-content flex flex-col flex-grow p-4">
                    <h1 class="font-bold text-2xl text-gray-700">Danh sách sản phẩm</h1>

                    <div
                        class="flex flex-col flex-grow border-4 border-gray-400 border-dashed bg-white rounded mt-4"
                        ></div>
                </div>
                <%@ include file="/layout/footer.jsp" %>
            </main>
        </div>
    </body>
</html>
